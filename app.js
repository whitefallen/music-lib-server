// Dotenv
const dotenv = require('dotenv');
dotenv.config();

const cors = require('cors');
const createError = require('http-errors');
const express = require('express');
//const path = require('path');

const spotifyRouter = require('./routes/spotify');

const app = express();
const router = express.Router();

// express setup
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// init Spotify API wrapper
const SpotifyWebApi = require('spotify-web-api-node');

let spotifyApi = new SpotifyWebApi({
    clientId : process.env.CLIENT_ID,
    clientSecret : process.env.CLIENT_SECRET,
});

spotifyApi.clientCredentialsGrant()
    .then(function(data) {
        // Save the access token so that it's used in future calls
        spotifyApi.setAccessToken(data.body['access_token']);
    }, function(err) {
        console.log('Something went wrong when retrieving an access token', err.message);
    });

// routes
app.set('spotify', spotifyApi)
router.use('/spotify', spotifyRouter);
app.use('/api', router);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.json({
    message: err.message,
    error: err
  });
});

module.exports = app;
