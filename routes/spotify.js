const express = require('express');
const router = express.Router();
const path = require('path');

/* GET home page. */
router.get('/', function(req, res, next) {
    res.sendFile(path.join(__dirname + '/../public/index.html'));
});

/* GET home page. */
router.get('/search', function(req, res, next) {
    res.sendFile(path.join(__dirname + '/../public/index.html'));
});

/* Spotify search route. */
router.get('/search/:artist', function(req, res, next) {
  // Search for a artist!
  let spotifyApi = req.app.get('spotify');
  let queryParam = req.params.artist;
  spotifyApi.searchArtists(`artist:${queryParam}`, {limit: 5})
      .then(function(data) {
          // Send the first (only) track object
          res.send(data.body);
      }, function(err) {
          console.error(err);
      });
});

/* Spotify search route. */
router.get('/debug', function(req, res, next) {
    // Search for a artist!
    res.send('hi');
});

module.exports = router;
